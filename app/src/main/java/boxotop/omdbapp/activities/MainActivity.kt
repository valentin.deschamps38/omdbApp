package boxotop.omdbapp.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.SearchRecentSuggestions
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import boxotop.omdbapp.R
import boxotop.omdbapp.apiClient.ApiClient
import boxotop.omdbapp.fragments.MovieDetailsFragment
import boxotop.omdbapp.fragments.MovieListFragment
import boxotop.omdbapp.model.MoviePreview
import boxotop.omdbapp.other.App
import boxotop.omdbapp.other.SuggestionProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

//todo toast before back leave app on list
//todo transition
//todo better search history view

class MainActivity : AppCompatActivity(), MovieListFragment.OnMovieListClickListener {

    private val loadingSpinner: ProgressBar by lazy {findViewById<ProgressBar>(R.id.progressBarSearch)}
    private val textViewHint: TextView by lazy {findViewById<TextView>(R.id.textViewHint)}
    private var searchItem: MenuItem? = null
    private val movieListFragment = MovieListFragment()
    private val movieDetailsFragment = MovieDetailsFragment()
    private lateinit var requestResultMovie: ApiClient.RequestResultMovie
    private var isDetailsDisplayed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        loadingSpinner.visibility = View.GONE
        textViewHint.text = App.resources.getString(R.string.hint_search)

        //view isn't created fast enough if the frag isn't displayed and hided before
        supportFragmentManager.beginTransaction().apply {
            add(R.id.fragmentContainer, movieDetailsFragment)
            hide(movieDetailsFragment)
            add(R.id.fragmentContainer, movieListFragment)
            hide(movieListFragment)
            commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if(!isDetailsDisplayed) {
            menuInflater.inflate(R.menu.topbarmain, menu)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)

            searchItem = menu?.findItem(R.id.action_search)
            val searchView = searchItem?.actionView as SearchView
            searchView.setSearchableInfo((getSystemService(Context.SEARCH_SERVICE) as SearchManager).getSearchableInfo(componentName))

            searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    searchItem?.collapseActionView()
                }
            }

            menu?.findItem(R.id.action_clear_history)?.setOnMenuItemClickListener {
                SearchRecentSuggestions(this, SuggestionProvider.AUTHORITY, SuggestionProvider.MODE).clearHistory()
                true
            }
        }else{
            menuInflater.inflate(R.menu.topbardetails, menu)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                if (isDetailsDisplayed){
                    supportFragmentManager.popBackStack()
                    setActionBarList()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun displayError(errorMessage:String){
        loadingSpinner.visibility = View.GONE
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun displayMovieList(moviePreviews:ArrayList<MoviePreview>){
        loadingSpinner.visibility = View.GONE

        movieListFragment.updateRecyclerView(moviePreviews)
        supportFragmentManager.beginTransaction().apply {
            show(movieListFragment)
            commit()
        }
    }

    private fun startSearch(title:String){
        //todo prevent doing the same search as the one displayed
        textViewHint.visibility = View.GONE
        loadingSpinner.visibility = View.VISIBLE

        GlobalScope.launch(Dispatchers.Main){
            val movies = GlobalScope.async {
                ApiClient().getMovieListByTitle(title)
            }.await()

            if(movies.isSuccessful){
                displayMovieList(movies.moviePreviewList)
            }else{
                displayError(movies.errorMessage)
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        //intent from searchView
        searchItem?.collapseActionView()
        setIntent(intent)
        if (intent != null) {
            handleIntent(intent)
        }
    }

    private fun handleIntent(intent: Intent){
        //when a search is performed
        if (Intent.ACTION_SEARCH == intent.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            //register the query in history
            saveSearchQuery(query)
            startSearch(query)
        }
    }

    private fun saveSearchQuery(query:String){
        val suggestions = SearchRecentSuggestions(this, SuggestionProvider.AUTHORITY, SuggestionProvider.MODE)
        suggestions.saveRecentQuery(query, null)
    }

    override fun onMovieListClick(movieId: String) {
        displayDetailsFragment(movieId)
    }

    private fun displayDetailsFragment(movieId: String){
        GlobalScope.launch(Dispatchers.Main){
            requestResultMovie = GlobalScope.async {
                ApiClient().movieDetailsById(movieId)
            }.await()

            if(requestResultMovie.isSuccessful) {
                movieDetailsFragment.updateDetails(requestResultMovie.movie!!)

                supportFragmentManager.beginTransaction().apply {
                    show(movieDetailsFragment)
                    replace(R.id.fragmentContainer, movieDetailsFragment)
                    addToBackStack(null)
                    commit()
                }

                if(!isDetailsDisplayed){setActionBarDetails()}
            }else{
                displayError(requestResultMovie.errorMessage)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isDetailsDisplayed){setActionBarList()}
    }

    private fun setActionBarDetails(){
        isDetailsDisplayed = true
        invalidateOptionsMenu()
    }

    private fun setActionBarList(){
        isDetailsDisplayed = false
        invalidateOptionsMenu()
    }
}
