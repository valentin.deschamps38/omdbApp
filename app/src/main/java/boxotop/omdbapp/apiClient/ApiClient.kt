package boxotop.omdbapp.apiClient

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import boxotop.omdbapp.R
import boxotop.omdbapp.model.Movie
import boxotop.omdbapp.model.MoviePreview
import boxotop.omdbapp.other.App
import com.google.gson.Gson
import org.json.JSONObject
import java.net.URL

class ApiClient {

    private val apiKey = App.resources.getString(R.string.omdbapikey) ?: ""
    private val urlBase = "http://www.omdbapi.com/?"
    private val urlApiKey = "apikey="
    private val urlSearch = "&s="
    private val urlTypeMovie = "&type=movie"
    private val urlPage = "&page="
    private val urlId = "&i="

    private val jsonResponse = "Response"
    private val jsonError = "Error"
    private val jsonSearch = "Search"
    private val jsonTotalResults = "totalResults"

    inner class RequestResultList {
        var isSuccessful: Boolean = false
        var moviePreviewList = ArrayList<MoviePreview>()
        var errorMessage = ""
    }
    inner class RequestResultMovie {
        var movie: Movie? = null
        var isSuccessful: Boolean = false
        var errorMessage = ""
    }

    private fun checkConnection(): Boolean{
        val cm = App.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnected ?: false
    }

    private fun getRequestResult(url:String):String {
        return URL(url).readText()
    }

    private fun buildUrlFromTitle(title:String, page:Int):String{
        return urlBase + urlApiKey + apiKey + urlSearch + title + urlTypeMovie + urlPage + page
    }

    private fun buildUrlFromId(id:String):String{
        return urlBase + urlApiKey + apiKey + urlId + id
    }

    private fun checkError(jsonObject: JSONObject):Boolean{
        return !jsonObject.getBoolean(jsonResponse)
    }

    fun getMovieListByTitle(title: String): RequestResultList {
        return searchByTitle(RequestResultList(), title,1)
    }

    private fun searchByTitle(requestResultList:RequestResultList, title:String, page:Int = 1): RequestResultList{
        if(checkConnection()) {
            val result = getRequestResult(buildUrlFromTitle(title, page))
            val jsonObject = JSONObject(result)

            if (checkError(jsonObject)) {
                requestResultList.isSuccessful = false
                requestResultList.errorMessage = jsonObject.getString(jsonError)
                return requestResultList
            } else {
                requestResultList.isSuccessful = true
                //extract movies from the result
                val movieJsonArray = jsonObject.getJSONArray(jsonSearch)
                //add them to movieListOld
                for (i in 0 until movieJsonArray.length()) {
                    requestResultList.moviePreviewList.add(Gson().fromJson(""+movieJsonArray[i], MoviePreview::class.java))
                }

                //if the number of movie added is inferior of what expected get the next page
                if (requestResultList.moviePreviewList.size < jsonObject.getInt(jsonTotalResults)) {
                    searchByTitle(requestResultList, title, page + 1)
                }
                return requestResultList
            }
        }else{
            requestResultList.apply {
                isSuccessful = false
                errorMessage = App.resources.getString(R.string.error_no_connection)
            }
            return requestResultList
        }
    }

    fun movieDetailsById(movieId:String): RequestResultMovie{
        return if(checkConnection()) {
            val result = getRequestResult(buildUrlFromId(movieId))
            RequestResultMovie().apply {
                isSuccessful = true
                movie = Gson().fromJson(result, Movie::class.java)
            }
        }else{
            RequestResultMovie().apply {
                isSuccessful = false
                errorMessage = App.resources.getString(R.string.error_no_connection)
            }
        }
    }
}