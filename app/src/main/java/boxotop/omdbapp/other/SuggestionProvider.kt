package boxotop.omdbapp.other

import android.content.SearchRecentSuggestionsProvider

class SuggestionProvider: SearchRecentSuggestionsProvider() {
    companion object {
        const val AUTHORITY = ".other.SuggestionProvider"
        const val MODE = SearchRecentSuggestionsProvider.DATABASE_MODE_QUERIES
    }

    init{
        setupSuggestions(AUTHORITY, MODE)
    }
}