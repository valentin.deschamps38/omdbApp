package boxotop.omdbapp.other

import android.app.Application
import android.content.Context
import android.content.res.Resources

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        private lateinit var instance: App

        val resources: Resources
            get() = instance.resources

        val context: Context
            get() = instance.applicationContext
    }
}