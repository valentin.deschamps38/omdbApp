package boxotop.omdbapp.adapter

import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import boxotop.omdbapp.R
import boxotop.omdbapp.fragments.MovieListFragment
import boxotop.omdbapp.model.MoviePreview
import com.bumptech.glide.Glide
import java.util.*

class MovieAdapter(private val movieListFragment: MovieListFragment): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private var moviePreviewList = ArrayList<MoviePreview>()
    private val listener: OnMoviePreviewClickListener = movieListFragment

    companion object {
        const val posterUrlNA = "N/A"
    }

    fun updateData(moviePreviews: ArrayList<MoviePreview>){
        moviePreviewList = moviePreviews
    }

    inner class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val textViewTitle: TextView = itemView.findViewById(R.id.textViewTitleCardMovie)
        private val textViewYear: TextView = itemView.findViewById(R.id.textViewYearCardMovie)
        private val poster: ImageView = itemView.findViewById(R.id.imageViewPosterCardMovie)
        private val layout: ConstraintLayout = itemView.findViewById(R.id.layoutCardMovie)
        private var currentMoviePosition = 0

        init {
            itemView.setOnClickListener {
                val moviePreview:MoviePreview = moviePreviewList[currentMoviePosition]
                listener.onMoviePreviewClick(moviePreview.imdbID, ViewCompat.getTransitionName(poster).toString())
            }
        }

        fun display(moviePreview: MoviePreview, position: Int) {
            currentMoviePosition = position
            textViewTitle.text = moviePreview.Title
            textViewYear.text = moviePreview.Year
            ViewCompat.setTransitionName(poster, moviePreview.imdbID)
            val posterUrl = moviePreview.Poster
            if (posterUrl != posterUrlNA){
                Glide.with(itemView).load(posterUrl).into(poster)
            }else{
                poster.setImageResource(R.drawable.ic_missing_image)
            }
            if(position%2 == 0){
                layout.setBackgroundColor(ContextCompat.getColor(movieListFragment.context!!, R.color.colorPrimary))
            }else{
                layout.setBackgroundColor(Color.TRANSPARENT)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapter.MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.movie_card, parent, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieAdapter.MovieViewHolder, position: Int) {
        holder.display(moviePreviewList[position], position)
    }

    override fun getItemCount(): Int {
        return moviePreviewList.size
    }

    interface OnMoviePreviewClickListener{
        fun onMoviePreviewClick(movieId: String, transitionName: String)
    }
}