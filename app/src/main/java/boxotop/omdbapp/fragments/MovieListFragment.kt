package boxotop.omdbapp.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import boxotop.omdbapp.R
import boxotop.omdbapp.adapter.MovieAdapter
import boxotop.omdbapp.model.MoviePreview

class MovieListFragment : Fragment(), MovieAdapter.OnMoviePreviewClickListener {

    private var listener: OnMovieListClickListener? = null
    private val recyclerAdapter: MovieAdapter by lazy { MovieAdapter(this) }
    private lateinit var recyclerViewMovie: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_movie_list, container, false)

        recyclerViewMovie = view!!.findViewById(R.id.recyclerViewMovie)
        recyclerViewMovie.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = recyclerAdapter
        }
        return view
    }

    fun updateRecyclerView(moviePreviews: ArrayList<MoviePreview>){
        recyclerViewMovie.layoutManager?.scrollToPosition(0)
        recyclerAdapter.updateData(moviePreviews)
        recyclerAdapter.notifyDataSetChanged()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnMovieListClickListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnMovieListClickListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onMoviePreviewClick(movieId: String, transitionName: String) {
        //tell activity to display detail fragment
        listener?.onMovieListClick(movieId)
    }

    interface OnMovieListClickListener {
        fun onMovieListClick(movieId: String)
    }
}
