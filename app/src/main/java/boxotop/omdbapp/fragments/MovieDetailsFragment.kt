package boxotop.omdbapp.fragments

import android.graphics.Paint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import android.widget.TextView
import boxotop.omdbapp.R
import boxotop.omdbapp.adapter.MovieAdapter
import boxotop.omdbapp.model.Movie
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_movie_details.*

class MovieDetailsFragment : Fragment() {
    private val starNumber = 5

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie_details, container, false)
    }

    fun updateDetails(movie: Movie){
        fillView(movie)
    }

    private fun getMovieData(movie: Movie): ArrayList<Pair<String, String>> {
        return ArrayList<Pair<String, String>>().apply {
            add(Pair(getString(R.string.label_year), movie.Year))
            add(Pair(getString(R.string.label_rated), movie.Rated))
            add(Pair(getString(R.string.label_runtime), movie.Runtime))
            add(Pair(getString(R.string.label_genre), movie.Genre))
            add(Pair(getString(R.string.label_writer), movie.Writer))
            add(Pair(getString(R.string.label_actors), movie.Actors))
            add(Pair(getString(R.string.label_plot), movie.Plot))
            add(Pair(getString(R.string.label_country), movie.Country))
            add(Pair(getString(R.string.label_awards), movie.Awards))
            add(Pair(getString(R.string.label_type), movie.Type))
            add(Pair(getString(R.string.label_boxoffice), movie.BoxOffice))
            add(Pair(getString(R.string.label_production), movie.Production))
            add(Pair(getString(R.string.label_website), movie.Website))
        }
    }

    private fun fillView(movie: Movie){
        layoutOtherDetails.removeAllViews()
        scrollViewDetails.fullScroll(ScrollView.FOCUS_UP)

        if(movie.Poster != MovieAdapter.posterUrlNA){
            Glide.with(this).load(movie.Poster).into(imageViewPoster)
        }else{
            imageViewPoster.setImageResource(R.drawable.ic_missing_image)
        }
        textViewTitle.apply {
            text = movie.Title
        }
        textViewReleasedLabel.apply {
            text = getString(R.string.label_released)
            paintFlags = Paint.UNDERLINE_TEXT_FLAG
        }
        textViewReleasedData.apply {
            text = movie.Released
        }
        textViewDirectorLabel.apply {
            text = getString(R.string.label_director)
            paintFlags = Paint.UNDERLINE_TEXT_FLAG
        }
        textViewDirectorData.apply {
            text = movie.Director
        }
        textViewRatingLabel.apply {
            text = getString(R.string.label_metascore)
            paintFlags = Paint.UNDERLINE_TEXT_FLAG
        }
        ratingBar.apply {
            numStars = starNumber
            setIsIndicator(true)
        }
        try {
            ratingBar.rating = Math.round(movie.Metascore.toFloat()/20).toFloat()
        }
        catch (e:NumberFormatException){
            ratingBar.rating = 0F
        }

        getMovieData(movie).forEach {
            val layoutDetail = layoutInflater.inflate(R.layout.movie_details_card, layoutOtherDetails, false)

            val textViewLabel = layoutDetail.findViewById<TextView>(R.id.textViewLabel)
            textViewLabel.apply {
                text = it.first
                paintFlags = Paint.UNDERLINE_TEXT_FLAG
            }
            val textViewData = layoutDetail.findViewById<TextView>(R.id.textViewData)
            textViewData.apply {
                text = it.second
            }
            layoutOtherDetails.addView(layoutDetail)
        }
    }
}
