package boxotop.omdbapp.model

data class MoviePreview(
    val Title: String,
    val Year: String,
    val imdbID: String,
    val Type: String,
    val Poster: String
)